import styled from 'styled-components'
import React from 'react'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'

import ImageButton from 'components/shared/ImageButton/ImageButton'

const Header = styled.header`
  height: 44px;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: ${props => props.theme.main.variables.zIndex.header};
  background-color: ${props => props.theme.main.background.header};
  display: flex;
  align-items: center;
  justify-content: center;
`
const AppHeader = ({
  store,
}) => {

  return (
    <Header>
      {
        store.selected &&
          <ImageButton
            src={store.selected.flagURL}
            alt={`${store.selected.countryCode} flag`}
            cursor='default'
          />
      }
    </Header>
  );
}

AppHeader.propTypes = {
  store: PropTypes.object.isRequired,
}

export default observer(AppHeader)
