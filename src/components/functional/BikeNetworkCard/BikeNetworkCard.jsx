import styled from 'styled-components'
// import './BikeNetworkCard.scss'
import React from 'react'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'
import H2 from 'components/shared/H2/H2'
import Wrapper from 'components/shared/Wrapper/Wrapper'
import TextInline from 'components/shared/TextInline/TextInline'
import CompaniesList from 'components/presentational/CompaniesList/CompaniesList'
import BikeAvailability from 'components/presentational/BikeAvailability/BikeAvailability'

const BikeNetworkCardWrapper = styled(Wrapper)`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px 15px;
  box-shadow: ${props => props.theme.main.extra.dropShadow};
  background-color: ${props => props.theme.main.background.light};
  
  .cardTitle {
    margin-bottom: 15px;
  }

  .extraInfo {
    align-self: flex-start;
    font-weight: 300;

    .companies {
      text-decoration: underline;
      margin-right: 8px;
    }
`
const BikeNetworkCard = ({
  network,
}) => {
  return (
    <BikeNetworkCardWrapper className='BikeNetworkCard'>
      <H2 className='cardTitle'>{network.city}</H2>
      <BikeAvailability
        percent={network.availability}
        thumbIcon={network.thumbIcon}
      />
      <Wrapper className='extraInfo'>
        <div>
          <TextInline className='companies'>Companies:</TextInline>
          <TextInline>{network.freeBikes} bikes/ {network.slots} slots</TextInline>
        </div>
        <CompaniesList companies={network.companies}/>
      </Wrapper>
    </BikeNetworkCardWrapper>
  );
}

BikeNetworkCard.propTypes = {
  network: PropTypes.shape({
    city: PropTypes.string,
    companies: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.arrayOf(PropTypes.string),
    ]),
    freeBikes: PropTypes.number,
    emptySlots: PropTypes.number,
    availability: PropTypes.string,
  }).isRequired,
}

export default observer(BikeNetworkCard)
