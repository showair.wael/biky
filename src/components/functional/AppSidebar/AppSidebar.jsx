import styled from 'styled-components'
import React from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

import List from 'components/shared/List/List'
import CountryListItem from 'components/presentational/CountryListItem/CountryListItem'

const Sidebar = styled.nav`
  width: 44px;
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  background-color: ${props => props.theme.main.background.sidebar};
  z-index: ${props => props.theme.main.variables.zIndex.sidebar};
  overflow-y: auto;
  &::-webkit-scrollbar{
    width: 0px;
    background: transparent;
  }
`
const AppSidebar = ({
  store,
}) => {
  
  return (
    <Sidebar>
      <List>
        {
          store.countries.map(({countryCode, flagURL}) => (
            <CountryListItem
              key={countryCode}
              countryCode={countryCode}
              flagURL={flagURL}
              onClick={()=>store.selectCountry(countryCode)}
            />)
          )
        }
      </List>
    </Sidebar>
  );
}

AppSidebar.propTypes = {
  store: PropTypes.object.isRequired,
}

export default observer(AppSidebar)
