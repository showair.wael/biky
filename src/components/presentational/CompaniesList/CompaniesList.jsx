import styled from 'styled-components'
import React from 'react'
import List from 'components/shared/List/List'
import ListItem from 'components/shared/ListItem/ListItem'

const CompaniesListStyled = styled(List)`
  margin-top: 10px;
  & li {
    margin-bottom: 5px;
  }
`

const CompaniesList = ({
  companies,
}) => {
  return companies && (
    <CompaniesListStyled>
      {
        companies.map((company, index) =>
          <ListItem key={`company-${index}`}>{company}</ListItem>
        )
      }
    </CompaniesListStyled>
  )
}
  
export default CompaniesList
