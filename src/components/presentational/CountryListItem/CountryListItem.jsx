import styled from 'styled-components'
import React from 'react'
import ListItem from 'components/shared/ListItem/ListItem'
import ImageButton from 'components/shared/ImageButton/ImageButton'

const CountryListItemView = styled(ListItem)`
  width: 44px;
  height: 44px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${props => !props.selected ? 'transparent' : props.theme.main.background.selected};
  &:hover {
    background-color: ${props => props.theme.main.background.hover};
  }
`

const CountryListItem = ({
  flagURL,
  countryCode,
  selected,
  onClick,
}) => {
  return(
    <CountryListItemView
      selected={selected}
      onClick={onClick}
    >
      <ImageButton
        src={flagURL}
        alt={`${countryCode} flag`}
      />
    </CountryListItemView>
  )
}

export default CountryListItem