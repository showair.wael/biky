import styled from 'styled-components'

const TextInline = styled.span`
  margin: 0;
  padding: 0;
`

export default TextInline
