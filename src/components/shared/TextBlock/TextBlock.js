import styled from 'styled-components'

const TextBlock = styled.p`
  margin: 0;
  padding: 0;
`

export default TextBlock
