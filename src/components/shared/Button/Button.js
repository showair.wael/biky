import styled from 'styled-components'

const Button = styled.button`
  width: 44px;
  height: 44px;
  border: 0;
`

export default Button
