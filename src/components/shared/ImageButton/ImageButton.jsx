
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Image from 'components/shared/Image/Image'
import Button from 'components/shared/Button/Button'

const ButtonWithImage = styled(Button)`
  padding-left: 4px;
  background: none;
  &:focus{
    outline: none;
  }

  img {
    width: 36px;
    height: 36px;
    &:hover {
      cursor: ${props => props.cursor};
    }
  }
`
const ImageButton = ({
  alt,
  src,
  onClick = () => {},
  cursor = 'pointer',
}) => {
  return (
    <ButtonWithImage onClick={onClick} cursor={cursor}>
      <Image src={src} alt={alt}/>
    </ButtonWithImage>
  );
}

ImageButton.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  cursor: PropTypes.string,
}

export default ImageButton
