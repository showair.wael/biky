const fromCanada = {
  bikeNetworkInCity_01: {
    company: ['Rideau Bikes', 'Bikes of Kanata'],
    href: '/v2/networks/ott-bike',
    id:'ott-bike',
    location:{
      country: 'CA',
      city: 'Ottawa',
      latitude: 37.8715809825,
      longitude: 23.7355544376,
    },
    name: 'abc',
  },
  bikeNetworkInCity_02: {
    company: ['Blue Jays Bikes'],
    href: '/v2/networks/toronto-bike',
    id: 'toronto-bike',
    location:{
      country: 'CA',
      city: 'Toronto',
      latitude: 37.1476704305,
      longitude: 22.8933729935,
      name:'def',
    },
  },
}

fromCanada.cityBikeNetwork = {
  withStations: {
    ...fromCanada.bikeNetworkInCity_01,
    stations: [
      { free_bikes: 4, empty_slots: 1 },
      { free_bikes: 5, empty_slots: 0 },
      { free_bikes: 2, empty_slots: 9 },
    ]
  },
  withNullableValuesInStation: {
    ...fromCanada.bikeNetworkInCity_01,
    stations: [
      { free_bikes: null, empty_slots: 5 },
      { free_bikes: 3, empty_slots: null },
    ]
  },
  withoutStations: {
    ...fromCanada.bikeNetworkInCity_01,
    stations: []
  }
}

const fromUK = {
  bikeNetworkInCity_01: {
    company: ['Sesame Bikes'],
    href: '/v2/networks/sesame-bike',
    id: 'sesame-bike',
    location:{
      country: 'UK',
      city: 'London',
      latitude: 45.73936412365,
      longitude: 11.0131747197,
      name:'some name',
    },
  }
}

const fromBrazil = {
  bikeNetworkInCity_01: {
    company: ['Mobilicidade Tecnologia LTD', 'Grupo Serttel LTDA'],
    href: '/v2/networks/sesame-bike',
    id: 'bikesantos',
    location:{
      country: 'BR',
      city: 'Santos',
      latitude: -23.954052,
      longitude: -46.333345,
      name:'BikeSantos',
    },
  }
}

export const DataSample = {
  fromCanada,
  fromUK,
  fromBrazil,
}
