import { ThemeProvider } from 'styled-components'
import './App.scss'
import React from 'react'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'

import AppHeader from './components/functional/AppHeader/AppHeader'
import AppSidebar from './components/functional/AppSidebar/AppSidebar'
import BikeNetworkCard from './components/functional/BikeNetworkCard/BikeNetworkCard'

const theme = {
  main: {
    background: {
      light: '#ffffff',
      dark: '#e7e7e7',
      header: '#0076a8',
      sidebar: '#2c3f50',
      hover: '#678678',
      selected: '#f26522',
    },
    text: {
      color: '#333333',
    },
    extra: {
      dropShadow: '0px 2px 8px #aaaaaa',
    },
    variables: {
      zIndex: {
        header: 1000,
        sidebar: 1001,
      }
    }
  }
};

const App = ({store}) => {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <AppHeader store={store}/>
          <main>
            <div className='main-content'>
                {
                  store.municipalNetworksOfBikes(store.selectedCountryCode).map(network => {
                    return <BikeNetworkCard network={network} key={network.id}/>
                  })
                }
              </div>
          </main>
        <AppSidebar store={store}/>
      </div>
    </ThemeProvider>
  )
}

App.propTypes = {
  store: PropTypes.object.isRequired,
}

export default observer(App)
