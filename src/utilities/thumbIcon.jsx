import React from 'react'
export const ThumbDownIcon = <span role='img' aria-label='low-availability'>&#x1f44e;</span>
export const ThumbUpIcon = <span role='img' aria-label='high-availability'>&#x1f44d;</span>
