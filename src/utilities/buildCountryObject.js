const FLAG_URL = `https://www.countryflags.io/$countryCode/flat/64.png`
const buildCountryObject = countryCode => ({
  countryCode,
  flagURL: FLAG_URL.replace('$countryCode', countryCode === 'UK' ? 'gb' : countryCode.toLowerCase())
})

export default buildCountryObject