const getMissingNetworkId = (url) => {
  const pattern = /\/v2\/networks\/(.*)\?fields=stations,id/g
  const matches = pattern.exec(url)
  return matches.length >= 2 ? matches[1] : null
}

export default getMissingNetworkId