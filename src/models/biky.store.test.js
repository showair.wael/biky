import { when } from 'mobx'
import BikyStore from './biky.store'
import { DataSample } from 'testing/data.samples'


const getNetworksSpy = jest.fn().mockImplementation(() => {
  return Promise.resolve({data:{
    networks: [
      DataSample.fromCanada.bikeNetworkInCity_01,
      DataSample.fromBrazil.bikeNetworkInCity_01,
    ],
  }})
})

const fetchBikeNetworkDetailsSpy = jest.fn().mockImplementation(() => {
  return Promise.resolve({data:{
    network: {
      ...DataSample.fromCanada.cityBikeNetwork.withStations,
    },
  }})
})

const MockBikeAPI = {
  getNetworks: getNetworksSpy,
  fetchBikeNetworkDetails: fetchBikeNetworkDetailsSpy,
}

const testEnvironment = {...MockBikeAPI}
  
describe('Biky Store', () => {
  describe('Given Biky Store is created', () => {
    afterEach(() => {
      jest.clearAllMocks()
    })

    it('should fetch all the networks from the server', done => {
      const store = BikyStore.create({networks: []}, testEnvironment)

      when(
        () => store.networks.length > 0,
        () => {
          expect(store.networks.length).toBe(2)
          expect(getNetworksSpy).toHaveBeenCalledTimes(1)
          done()
        }
      )

    })

    it('should auto select first network from the server response', done => {
      const store = BikyStore.create({networks: []}, testEnvironment)

      when(
        () => store.selectedCountryCode,
        () => {
          expect(store.selectedCountryCode).toBe('CA')
          done()
        }
      )

    })

    it('should fetch all the networks of the selected country', done => {
      const store = BikyStore.create({networks: []}, testEnvironment)

      when(
        () => store.networks.length>0 && store.networks[0].stations.length>0,
        () => {
          expect(store.networks[0].stations).toEqual([
            { free_bikes: 4, empty_slots: 1 },
            { free_bikes: 5, empty_slots: 0 },
            { free_bikes: 2, empty_slots: 9 },
          ])
          expect(fetchBikeNetworkDetailsSpy).toHaveBeenCalledTimes(1)
          done()
        }
      )
    })
  })
})
