import { BikyModel } from './biky.models'
import { DataSample } from 'testing/data.samples'
import { ThumbUpIcon, ThumbDownIcon } from '../utilities/thumbIcon'

jest.mock('../BikeAPI/BikeAPI')

describe('BikyModel', () => {
  it('can create model', () => {
    const bikyModel = BikyModel.create({networks: [
      DataSample.fromCanada.bikeNetworkInCity_01,
      DataSample.fromCanada.bikeNetworkInCity_02,
      DataSample.fromBrazil.bikeNetworkInCity_01,
    ]})
    
    expect(bikyModel.networks.length).toBe(3)
  })

  it('can compute countries without repetition', () => {
    const bikyModel = BikyModel.create({networks: [
      DataSample.fromCanada.bikeNetworkInCity_01,
      DataSample.fromCanada.bikeNetworkInCity_02,
    ]})
    
    expect(bikyModel.countries).toEqual([
      {
        countryCode: 'CA',
        flagURL: 'https://www.countryflags.io/ca/flat/64.png',
      },
    ])
  })
  
  it('can compute country object for UK', () => {
    const bikyModel = BikyModel.create({networks: [
      DataSample.fromUK.bikeNetworkInCity_01,
    ]})

    expect(bikyModel.countries).toEqual([{
      countryCode: 'UK',
      flagURL: 'https://www.countryflags.io/gb/flat/64.png'
    }])
  })

  it('can group networks per country', () => {
    const bikyModel = BikyModel.create({networks: [
      DataSample.fromCanada.bikeNetworkInCity_01,
      DataSample.fromCanada.bikeNetworkInCity_02,
      DataSample.fromBrazil.bikeNetworkInCity_01,
      DataSample.fromUK.bikeNetworkInCity_01,
    ]})
    const networksInCanada = bikyModel.nationalNetworksOfBikes('CA')

    expect(networksInCanada.length).toBe(2)
    expect(networksInCanada[0].location.country).toEqual('CA')
    expect(networksInCanada[1].location.country).toEqual('CA')
  })

  it('can compute city info for bike network having stations', () => {
    const bikyModel = BikyModel.create({networks: [
      DataSample.fromCanada.cityBikeNetwork.withStations,
    ]})
    const groupedNetworksByCity = bikyModel.municipalNetworksOfBikes('CA')

    expect(groupedNetworksByCity).toEqual([{
      city: 'Ottawa',
      companies: ['Rideau Bikes', 'Bikes of Kanata'],
      id: 'ott-bike',
      freeBikes: 11,
      slots: 21,
      availability: '52%',
      thumbIcon: ThumbUpIcon,
    }])
  })

  it('can compute city info for bike network having stations with nullable values', () => {
    const bikyModel = BikyModel.create({networks: [
      DataSample.fromCanada.cityBikeNetwork.withNullableValuesInStation,
    ]})
    const groupedNetworksByCity = bikyModel.municipalNetworksOfBikes('CA')

    expect(groupedNetworksByCity).toEqual([{
      city: 'Ottawa',
      companies: ['Rideau Bikes', 'Bikes of Kanata'],
      id: 'ott-bike',
      freeBikes: 3,
      slots: 8,
      availability: '37%',
      thumbIcon: ThumbUpIcon,
    }])
  })

  it('can compute city info for bike network having no stations', () => {
    const bikyModel = BikyModel.create({networks: [
      DataSample.fromCanada.cityBikeNetwork.withoutStations,
    ]})
    const groupedNetworksByCity = bikyModel.municipalNetworksOfBikes('CA')

    expect(groupedNetworksByCity).toEqual([{
      city: 'Ottawa',
      companies: ['Rideau Bikes', 'Bikes of Kanata'],
      id: 'ott-bike',
      freeBikes: 0,
      slots: 0,
      availability: '0%',
      thumbIcon: ThumbDownIcon,
    }])
  })

  it('can compute an object for the selected country', () => {
    const bikyModel = BikyModel.create({
      networks: [
        DataSample.fromBrazil.bikeNetworkInCity_01,
        DataSample.fromCanada.bikeNetworkInCity_01,
      ],
      selectedCountryCode: 'CA',
    })

    expect(bikyModel.selected).toEqual({
      countryCode: 'CA',
      flagURL: 'https://www.countryflags.io/ca/flat/64.png',
    })
  })
})
