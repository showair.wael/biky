import { types } from 'mobx-state-tree'
import buildCountryObject from 'utilities/buildCountryObject'
import { ThumbDownIcon, ThumbUpIcon } from 'utilities/thumbIcon'

const BikeNetworkStation = types.model({
  empty_slots: types.maybeNull(types.number),
  free_bikes: types.maybeNull(types.number),
})
  .named('BikeNetworkStation')

const BikeNetworkLocation = types.model({
  city: types.string,
  country: types.string,
  latitude: types.number,
  longitude: types.number,
  name: types.maybe(types.string),
})
  .named('BikeNetworkLocation')

const BikeNetwork = types.model({
  company: types.maybeNull(types.union(types.string, types.array(types.string))),
  href: types.string,
  id:types.string,
  location: BikeNetworkLocation,
  stations: types.optional(types.array(BikeNetworkStation), [])
})
  .named('BikeNetwork')

export const BikyModel = types.model({
  networks: types.array(BikeNetwork),
  selectedCountryCode: types.maybe(types.string),
})
  .named('BikyModel')

  .views(self => ({
    get countries(){
      const countriesSet = new Set(self.networks.map(network => network.location.country))
      return [...countriesSet].map(buildCountryObject)
    },

    nationalNetworksOfBikes(country) {
      return self.networks.filter(network => network.location.country === country)
    },

    municipalNetworksOfBikes (countryCode) {
      return self.nationalNetworksOfBikes(countryCode)
        .map(network => {
          const initial = {freeBikes: 0, slots: 0}
          const {freeBikes, slots} = network.stations.reduce((results, station) => {
            return {
              freeBikes: results.freeBikes + station.free_bikes,
              slots: results.slots + station.empty_slots + station.free_bikes,
            }
          }, initial)

          const percent = freeBikes/slots * 100
          const availability = (isFinite(percent) ? Math.floor(percent) : 0)
          const thumbIcon = availability > 25 ? ThumbUpIcon : ThumbDownIcon
          const companies = !network.company
              ? null : Array.isArray(network.company)
                ? network.company : [network.company]

          return {
            'city': network.location.city,
            companies,
            'id': network.id,
            freeBikes,
            slots,
            availability: availability + '%',
            thumbIcon,
          }
        })
    },

    get selected(){
      if(self.networks.length === 0) return undefined
      const countryCode = self.selectedCountryCode || self.networks[0].location.country
      return buildCountryObject(countryCode)
    },   
  }))
 
export default BikyModel
