import { flow, onSnapshot, getEnv } from 'mobx-state-tree'
import BikyModel from './biky.models'
import getMissingNetworkId from 'utilities/getMissingNetworkId'

const BikyStore = BikyModel
  .actions(self => ({
    afterCreate(){
      const dispose = onSnapshot(self.networks, () => {
        if(self.networks.length === 0) { return }
        self.selectCountry(self.networks[0].location.country)
        dispose()
      })
      
      self.loadAllNetworksOfBikes()
    },

    loadAllNetworksOfBikes: flow(function * (){
      try{
        const response = yield getEnv(self).getNetworks()
        self.networks = response.data.networks
      } catch(error) {
        //TODO: handle error.
      }
    }),
  
    selectCountry(countryCode){
      self.selectedCountryCode = countryCode
      self.fetchNationalNetworksDetails(countryCode)
    },
  
    fetchNationalNetworksDetails: flow (function * (countryCode) {
      const nationalNetworksOfBikes = self.nationalNetworksOfBikes(countryCode)
      const threads = []
  
      nationalNetworksOfBikes.forEach(({href: endpoint}) => {
        threads.push(getEnv(self).fetchBikeNetworkDetails(`${endpoint}?fields=stations,id`))
      });
  
      for(let i=0; i<threads.length; i++){
        try {
          const response = yield threads[i]
          const {id, stations} = response.data.network
          
          //NOTE: I have to account for weird situation when endpoint randomly does not return network id in the response
          const derivedId = id || getMissingNetworkId(response.config.url)
  
          const network = nationalNetworksOfBikes.find(network => network.id === derivedId)
  
          network.stations = stations
        } catch(error){
          //TODO: Handle error.
        }
      }
    }),
  }))
  

export default BikyStore
