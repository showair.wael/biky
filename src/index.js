import './index.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import BikyStore from './models/biky.store'
import BikeAPI from './BikeAPI/BikeAPI'

const initialState = { networks: [] }
const environment = {
  getNetworks: BikeAPI.getNetworks,
  fetchBikeNetworkDetails: BikeAPI.fetchBikeNetworkDetails,
}

let store = BikyStore.create(initialState, environment)

ReactDOM.render(
  <React.StrictMode>
    <App store={store}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
