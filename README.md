Ready to meet Biky? One of the best new cycling apps in the market :)

## Development
- This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
- I did not know anything about MobX before. But I decided to give it a try.
-  I styled the app first using `scss` files & `classnames`library. By the end of project, I learnt more about `styled components` and I love it. It helps in having a more readable HTML code structure. Thanks for suggesting it for me.
- I designed the app state such `detailed network data = summary network data + extra things`. That was a key point in simplifying the state otherwise, i might have needed to use a MobX `reference`which is a little bit an overkill for this app simple data.
- I realize that the amount of data is huge while launching the app. Roughly 645 entry in an array. So i decided to filter all the fields that I only need. This helps in reducing the response size for each network.
- I care about the readability of my code. So you will find some long names. IMHO, it is better to be more expressive in the code.
- I used generator functions to implement the asynchornous communication with the Backend. It is powerful, testable functions and it works well with Mobix.
- To display the all the cards for every bike network in a country, I decided to send **parallel** HTTP requests to the backend then I wait till receiving all responses before moving forwarding and rendering them to the user.

## Code/Folder Structure

- `BikeAPI`: This is a module that is responsible for any backend related communication for the `Bike` endpoint.
- `components`: This is a folder that holds all my visual pieces in the screen. I divide them in 3 categories: 
  - `functional`: This is for any functional components that are smart and aware of the app context.
  - `presentation`: This is for any presentational components that are dumb and not aware of the app context. You just pass some `props` to it and it renders.
  - `shared`: This is for any generic component that can be used in either previous components. Something like `Button`, `Image`
 - `models`: This is for Mobx related `models`,`actions`, `views`..etc
 - `sccs`: This is to apply some global styles across the app, like `reset.scss` to reset the default browser styles for the elements.
 - `testing`: Usually, i add here reusable mocks and data samples that I might need in the unit tests. 
 - `utilities`: This is to have common small reusable pieces of auxiliary code. 

## Testing  
- Anything related to Mobx `models`, `actions` is unit tested as early as it could be in the app development cycle. This helps me a lot in maintaining the required behaviors as I changed my thoughts around the app state and how it would be consumed in the app.
- At any point of time if I feel like Mobx related code is not testable, I change my code to be more testable.
- I decided to use `axios` package as my way of communication with the backend. It gives me easy of use and customization and it is testable.


## What could I have done better
- Checking the endpoint response earlier before thinking about what the app state could look like.
- To  get list of countries, I had to loop through every entry in an array of 645 elements. I could have transformed this array to a different data structure like `Object` or `Map` so that I can obtain the list of countries faster. Same data structure would help in getting networks of a given country faster.



## Available Scripts

  

In the project directory, you can run:

  

### `yarn start`

  

Runs the app in the development mode.<br  />

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

  

The page will reload if you make edits.<br  />

You will also see any lint errors in the console.

  

### `yarn test`

  

Launches the test runner in the interactive watch mode.<br  />

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.